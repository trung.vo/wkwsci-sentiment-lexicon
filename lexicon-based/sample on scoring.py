from nltk.classify import NaiveBayesClassifier
from nltk.corpus import subjectivity
from nltk.corpus import sentence_polarity
from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from sklearn.metrics import classification_report
from nltk.sentiment.util import *
from sklearn.metrics import accuracy_score
#
# data with label
n_instances = 5000
neg_docs = [(sent, 'neg') for sent in sentence_polarity.sents(categories='neg')[:n_instances]]
pos_docs = [(sent, 'pos') for sent in sentence_polarity.sents(categories='pos')[:n_instances]]

# print("len(sentence_polarity.sents())")
# print(len(sentence_polarity.sents()))
#
# print("len(neg_docs), len(pos_docs)")
# print(len(neg_docs), len(pos_docs))

sid = SentimentIntensityAnalyzer()
predictedResult = []
TP = 0
TN = 0
FP = 0
FN = 0

for i in range(0,2):

    sentence = ''
    for x in range(0, len(neg_docs[i][0])):
        sentence = sentence + neg_docs[i][0][x] + ' '

    print(sentence)
    ss = sid.polarity_scores(sentence)
    print(ss)
    print("---------------------")