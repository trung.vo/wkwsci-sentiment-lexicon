from nltk.classify import NaiveBayesClassifier
from nltk.corpus import subjectivity
from nltk.corpus import sentence_polarity
from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from sklearn.metrics import classification_report
from nltk.sentiment.util import *
import csv
import numpy as np
from sklearn.metrics import accuracy_score
#
# data with label
n_instances = 5000
neg_docs = [(sent, 'neg') for sent in sentence_polarity.sents(categories='neg')[:n_instances]]
pos_docs = [(sent, 'pos') for sent in sentence_polarity.sents(categories='pos')[:n_instances]]


sid = SentimentIntensityAnalyzer()
predictedResult = []
TP = 0
TN = 0
FP = 0
FN = 0


sid = SentimentIntensityAnalyzer()
predictedResult = []
labelledResult = []
with open('C://Users//Trung Vo//Dropbox//NTU year 2//CI//code//yelp.csv') as yelpcsv:
    yelpData = csv.reader(yelpcsv, delimiter=',')


    for row in yelpData:
        ss = sid.polarity_scores(row[0])
        if ss['compound'] > 0.2:
            predictedResult.append('positive')
        else:
            predictedResult.append('negative')
        labelledResult.append(row[1])

target_names = ['negative', 'positive']
print(classification_report(labelledResult, predictedResult, target_names=target_names))
print("accuracy_score: ", accuracy_score(predictedResult, labelledResult))